#!/bin/bash
# [En]Brother install-top script
#
# 	Look at the current system language 
#   and launch the install-fr script if we are in French,
#   otherwise the install-en English one (by default)

# [Fr]Top script pour l'installation des imprimantes Brother
#
#	Examine la langue du système, 
#   lance le script install-fr si celle-ci est le français,
#   et sinon lance le script anglais install-en (par défault)

# by Yves Saboret from the Emmabuntüs collective

echo "Brother printers (top) install script."

cur_lang=${LANG}
cod_lang="${cur_lang:0:3}"
#echo "Code Lang = ${cod_lang}"
case $cod_lang in
  fr_)
    echo "  On parle français."
    source /opt/Brother/install_fr.sh
    ;;
  *)
    echo "  Using English language."
    source /opt/Brother/install_en.sh
    ;;
esac

